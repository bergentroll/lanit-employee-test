#!/bin/bash

# © CC0 2018 Антон Карманов <bergentroll@insiberia.net>

# Данный скрипт является ответом на пп. 1-3 технического задания для
# соискателя на вакансию системного администратора в команию ООО «ЛАНИТ-Урал».

# Скрипт предназначен для работы с ОС Astra Linux CE 2.11.5. В качестве средств
# виртуализации использовалась библиотека libvirt и гипервизор QEMU/KVM.
# Применялась следующая конфигурация сети:

# <network>
#   <name>astra-net</name>
#   <forward mode='nat'></forward>
#   <bridge stp='on' delay='0'/>
#   <domain name='astra-net'/>
#   <ip address='192.168.100.1' netmask='255.255.255.0'></ip>
# </network>

# Сервер устанавливался в ручном режиме с набором "Базовые средства". Для такой
# установки необходимо произвести первоначальную настройку сети, а именно:

# 1. Добавить следующие строки в файл /etc/network/interfaces:
# 
# auto eth0
# iface eth0 inet static
#   address 192.168.100.2
#   netmask 255.255.255.0
#   gateway 192.168.100.1
#   dns-nameservers 192.168.100.1

# 2. Создать файл /etc/resolv.conf и добавить в него следующие строки:
# nameserver 127.0.0.1
# nameserver 192.168.100.1

# Таким образом, DNS-демон будет обращаться сначала к локальному кешу и таблице
# DHCP-лиз, а затем к вышестоящему серверу (гипервизору).

# 3. Перезапустить сетевой демон командой:
# systemctl restart networking

# Требуется наличие в приводе сервера установочного диска Astra Linux.

# Вместо предлагаемых документацией пакетов tftp-hpa и apache2 используется
# tftp-сервер dnsmasq и вёб-сервер lighttpd, что экономит ресурсы сервера без
# регрессий.

# Данный скрипт и перечисленные в комментариях команды должны запускаться с
# правами суперпользователя.

### Предварительные проверки ###
ASTRA_VERSION=$(cat /etc/astra_version)
if [ "$ASTRA_VERSION" != 'CE 2.11.5 (orel)' ]; then
  printf 'Версия дистрибутива не соотвествует требуемой'
  printf ' (ASTRA LINUX CE 2.11.5)\n\nВыход\n'
  exit
fi

if [ "$EUID" -ne 0 ]; then
  printf 'Скрипт должен быть запущен с правами суперпользователя\n'
  printf '\nВыход\n'
  exit
elif [ -f /etc/kickstart-true ]; then
  printf 'Скрипт на этой машине уже выполнен успешно\n\nВыход\n'
  exit
elif [ -f /etc/kickstart-dirty ]; then
  printf 'Скрипт на этой машине выполнялся и не завершил работу или'
  printf ' выполняется в данный момент.\n'
  printf 'Восстановите конфигурационные файлы, удалите /etc/kickstart-dirty'
  printf ' и запустите скрипт заново.\n\nВыход\n'
  exit
fi

# Если файл существует, скрипт запускался, но не завершился
touch /etc/kickstart-dirty

### Настройка DNS и DHCP ###

# Установка необходимых пакетов для пп. 1-2 задания
apt update
apt -y install patch dnsmasq lighttpd

# Настраиваем мнонтирование DVD для использования в качестве репозитория
umount -d /media/cdrom/ 2> /dev/null
mount -o ro,gid=www-data /dev/cdrom /media/cdrom/
sed -i '/media\/cdrom/d' /etc/fstab
echo '# Mount installation DVD' >> /etc/fstab
echo '/dev/cdrom /media/cdrom/ iso9660 ro,gid=www-data 0 0' >> /etc/fstab

# Настройка директории tftp-сервера
mkdir -p /srv/tftp/
cp /media/cdrom/netinst/{initrd.gz,linux,pxelinux.0} /srv/tftp/
cp /media/cdrom/isolinux/ldlinux.c32 /srv/tftp/
chown -R dnsmasq:dip /srv/tftp/
mkdir /srv/tftp/pxelinux.cfg/

# Настройка директории локального репозитория
mkdir -p /srv/repo/astra/
ln -s /media/cdrom/{dists,pool} /srv/repo/astra/
chown -R www-data:www-data /srv/repo/

# Конфигурация для сетевой установки.
# Присутствует строка, позволяющая работать с неподписанными репозиториями.
# По какой-то причине разработчики дистрибутива не включили открытый ключ
# репозитория на диске в RAM-диск, предназначенный для загрузки по сети.
# Корректым решением было бы перепаковать RAM-диск, добавив ключ вручную,
# однако я не использую здесь этот способ из-за громоздкости.
cat > /srv/tftp/pxelinux.cfg/default << EOF
DEFAULT astra

LABEL astra
kernel linux
append initrd=initrd.gz debian-installer/allow_unauthenticated=true

TIMEOUT 1
EOF

# Редактирование конфигурации dnsmasq
sed -i '0,/#\(cache-size\)=.*/s//\1=1000/'              /etc/dnsmasq.conf
sed -i '0,/#\(no-negcache\).*/s//\1/'                   /etc/dnsmasq.conf
sed -i '0,/#\(dhcp-range\)=.*/s//\1=192.168.100.3,192.168.100.100,12h/' \
                                                        /etc/dnsmasq.conf
sed -i '0,/#\(dhcp-boot\)=.*/s//\1=pxelinux.0/'         /etc/dnsmasq.conf
sed -i '0,/#\(dhcp-option\)=3.*/s//\1=3,192.168.100.1/' /etc/dnsmasq.conf
sed -i '0,/#\(enable-tftp\).*/s//\1/'                   /etc/dnsmasq.conf
sed -i '0,/#\(tftp-root\)=.*/s//\1=\/srv\/tftp\//'      /etc/dnsmasq.conf

# Редактирование конфигурации lighttpd
sed -i '0,/\(server.document-root.*=\).*/s//\1 "\/srv\/repo\/"/' \
                                             /etc/lighttpd/lighttpd.conf
sed -i '/document-root.*/a server.dir-listing          = "enable"' \
                                             /etc/lighttpd/lighttpd.conf

# Перезапуск демонов
systemctl restart dnsmasq lighttpd

### Создание локального репозитория ###

# Утилита, значительно облегчающая запуск репозитория
apt install -y reprepro

# Создание криптографического ключа
chmod 700 /root/
export GNUPGHOME='/root/'

gpg --batch --yes --gen-key << EOF
%echo Генерация ключа для репозитория
Key-Type: RSA
Key-Length: 2048
Key-Usage: sign
Name-Real: Astra Linux user
Name-Email: user@example.com
Expire-Date: never
%no-protection
%commit
%echo Выполнено
EOF

# Получаем идентификатор созданного ключа
KEY_ID=$(gpg --list-keys | grep -e '[A-Z0-9]\{40\}' | tail -n1 | xargs)

mkdir -p /srv/repo/custom/conf/

# Добавляем в корень репозитория открытый публичный ключ.
# В дальнейшем необходимо будет получить его на клиентах и добавить командой
# apt-key add key.pub.
gpg --default-key $KEY_ID --armor --export > /srv/repo/custom/key.pub

# Конфигурация репозитория
cat > /srv/repo/custom/conf/distributions << EOF
Origin: Debian
Label: Custom Astra Linux repo
Codename: orel
Architectures: i386
Components: contrib
Contents:
SignWith: yes
Description: Wine packages
EOF

# Инициализируем подписанный репозиторий
reprepro --gnupghome='/root/' --ask-passphrase -b /srv/repo/custom/ export

# Получаем пакет PlayOnLinux
cd /tmp/
wget https://www.playonlinux.com/script_files/PlayOnLinux/4.2.12/\
PlayOnLinux_4.2.12.deb

# Добавляем полученный пакет в репозиторий
reprepro --gnupghome='/root/' --ask-passphrase -b /srv/repo/custom/ \
  includedeb orel PlayOnLinux_4.2.12.deb

# Делаем вёб-сервер владельцем репозитория
chown -R www-data:www-data /srv/repo/

# Удаляем индикатор "грязного" выполнения
rm /etc/kickstart-dirty

# Создаём индикатор успешного выполнения.
# Наличие данного файла свидетельствует о выполнении всех команд скрипта,
# но не гарантирует их безошибочность.
touch /etc/kickstart-true
